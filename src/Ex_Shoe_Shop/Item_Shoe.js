import React, { Component } from 'react'

export default class Item_Shoe extends Component {

    render() {
        console.log(this.props);
        let { id, image, name, price, shortDescription } = this.props.item;
        return (

            <div className="col-3 p-4">
                <div className="card">
                    <img src={image} className="card-img-top" alt="..." />
                    <div className="card-body">
                        <h5 className="card-title">{name}</h5>
                        <h6>$ {price}</h6>
                        <p className="card-text">{shortDescription}</p>
                        <a href="#" onClick={() => {
                            this.props.handleOnclick(this.props.item)
                        }} className="btn btn-primary">Add to cart</a>
                    </div>
                </div>
            </div>



        )
    }
}
