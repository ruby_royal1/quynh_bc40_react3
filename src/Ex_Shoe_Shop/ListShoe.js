import React, { Component } from 'react'
import Item_Shoe from './Item_Shoe'
export default class ListShoe extends Component {
    render() {
        console.log('listShoe', this.props);
        return (

            <div className="row">

                {this.props.list.map((shoe) => {
                    return <Item_Shoe handleOnclick={this.props.handleAddToCart} item={shoe} />
                })}
            </div>

        )
    }
}
