import React, { Component } from 'react'

export default class Cart extends Component {
    renderTbody = () => {
        return this.props.cart.map((item) => {
            return (
                <tr>
                    <td>{item.id}</td>
                    <td>{item.name}</td>
                    <td>{item.price * item.soLuong}</td>
                    <td>
                        <button className='btn btn-danger rounded-circle ' ><i class="fa fa-minus"></i></button>
                        <span className='mx-3'>{item.soLuong}</span>
                        <button className='btn btn-success rounded-circle ' ><i class="fa fa-plus"></i></button>
                    </td>
                    <td><img src={item.image} alt="" style={{ width: 40 }} /></td>
                </tr>
            )
        })


    }
    render() {
        return (
            <div>
                <table className="table">
                    <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Price</th>
                        <th>Quantity</th>
                        <th>Img</th>
                    </thead>
                    <tbody>
                        {this.renderTbody()}
                    </tbody>
                </table>
            </div>
        )
    }
}
