import React, { Component } from 'react'
import Cart from './Cart'
import { dataShoe } from './data_shoe'
import ListShoe from './ListShoe'

export default class Ex_Shoe_Shop extends Component {
    state = {
        listShoe: dataShoe,
        cart: [],
    }
    handleAddToCart = (shoe) => {
        let cloneCart = [...this.state.cart];

        let index = cloneCart.findIndex((item) => {
            return item.id === shoe.id
        })
        // TH1: Sản phẩm chưa có trong giỏ hàng thì push
        if (index == -1) {
            let newShoe = { ...shoe, soLuong: 1 }
            cloneCart.push(newShoe)
        } else {
            cloneCart[index].soLuong++;
        }

        // TH2: Sản phẩm đã có trong giỏ hàng thì update số lượng


        this.setState({
            cart: cloneCart
        })
    }

    handleChangeQuantity = (id, soLuong) => {
        //   soluong:1 hoặc -1
    }

    handleRemoveShoe = (id) => {
        //   index
        // splice(index,1)
    }
    render() {
        return (
            <div className='container'>
                {/* props.children nội dung nằm giữa 2 thẻ */}
                <h2>{this.props.children}</h2>
                <Cart cart={this.state.cart} />
                <ListShoe handleAddToCart={this.handleAddToCart} list={this.state.listShoe} />

            </div>
        )
    }
}
